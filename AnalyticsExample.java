package com.bright.hub;
 
import android.R;
import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageView;
 
import com.google.android.apps.analytics.GoogleAnalyticsTracker;
 
public class AnalyticsExample extends Activity {
    /** Called when the activity is first created. */
        private GoogleAnalyticsTracker exampleTracker;
        
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);
        
                exampleTracker = GoogleAnalyticsTracker.getInstance();
                
                //Use one of this tracker initializers. 
                exampleTracker.start("UA-12345678-1", this); //With no timer 
                exampleTracker.start("UA-12345678-1", 20, this); //With timer
 
                exampleTracker.trackPageView("/AnalyticsExample");
 
                ImageView myImage = (ImageView) findViewById(R.id.someImage); 
                myImage.setOnClickListener(new OnClickListener() {
                        @Override
                        public void onClick(View v) {
                                exampleTracker.trackEvent("Clicks", // Category
                                                "On Image", // Action
                                                "Some image in my App", // Label
                                                1); // Value
                        }
                });
                
                //If you have not used the timer tracker, you need to dispatch the data.
                exampleTracker.dispatch();
        
    }
}